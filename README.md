# Digital alarm clock 
This project is part of the requirements for project

`Design and implementation of IoT digital alarm clock using microcontroller`

Student name: Abdullah Adel Ahmed Alsigar

Student number: 915775

University of Portsmouth

## How to run
This project is build on the latest ESP-IDF "at the time of writing"

You have to setup ESP-IDF toolchain, connect the development board and other components
 as described in the project report.