#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_spi_flash.h"
#include "esp_event.h"
#include "nvs_flash.h"
#include "driver/touch_pad.h"

#include "display_app.h"
#include "state_app.h"
#include "smart_config_app.h"
#include "sntp_app.h"
#include "audio_app.h"
#include "http_server_app.h"

void app_main() {
//    Create default event loop.
    ESP_ERROR_CHECK(esp_event_loop_create_default());
//    ESP_ERROR_CHECK(nvs_flash_erase()); // just for testing reset chip memory
    ESP_ERROR_CHECK(nvs_flash_init());
//
//     Initialize touch pad peripheral.
//     The default fsm mode is software trigger mode.
    ESP_ERROR_CHECK(touch_pad_init());
//     Set reference voltage for charging/discharging
//     In this case, the high reference valtage will be 2.7V - 1V = 1.7V
//     The low reference voltage will be 0.5
//     The larger the range, the larger the pulse count value.
    touch_pad_set_voltage(TOUCH_HVOLT_2V7, TOUCH_LVOLT_0V5, TOUCH_HVOLT_ATTEN_1V);
    touch_pad_config(TOUCH_PAD_NUM8, 0);
//  Initialize the application state
    state_app_init();
//   Create and start the tasks
    xTaskCreate(smart_config_app, "smart_config_task", 4096, NULL, 1, NULL);
    xTaskCreate(sntp_app, "sntp_task", 4096, NULL, 1, NULL);
    xTaskCreate(audio_app, "audio_task", 4096, NULL, 1, NULL);
    xTaskCreatePinnedToCore(display_app, "display_app", 4096 * 2, NULL, 0, NULL, 1);
    xTaskCreatePinnedToCore(http_server_app, "http_server_app", 10240 * 2, NULL, 0, NULL, 1);

//  Keep the loop running, log the app state for debugging
    while (1) {
        state_app_log();
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}
