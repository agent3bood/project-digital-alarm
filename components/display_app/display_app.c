#include "display_app.h"
#include "state_app.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <lvgl/src/lv_core/lv_style.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_freertos_hooks.h"
#include "freertos/semphr.h"

#include "esp_system.h"
#include "driver/gpio.h"

// Littlevgl specific include
#include "lvgl/lvgl.h"
#include "lvgl_driver.h"

static void lv_tick_task(void *arg);

//Creates a semaphore to handle concurrent call to lvgl stuff
//If you wish to call *any* lvgl function from other threads/tasks
//you should lock on the very same semaphore!
SemaphoreHandle_t xGuiSemaphore;


// Array for lvgl objects, we have 3 labels
lv_obj_t *lvgl_objects[3];

static void lvgl_task(lv_task_t *t);

// Main display task
void display_app(void *pvParameters) {
    xGuiSemaphore = xSemaphoreCreateMutex();
//  Initialize lvgl
    lv_init();
//  Initialize TFT driver
    lvgl_driver_init();

    static lv_color_t buf1[DISP_BUF_SIZE];
    static lv_color_t buf2[DISP_BUF_SIZE];
    static lv_disp_buf_t disp_buf;
    lv_disp_buf_init(&disp_buf, buf1, buf2, DISP_BUF_SIZE);

    lv_disp_drv_t disp_drv;
    lv_disp_drv_init(&disp_drv);
    disp_drv.flush_cb = disp_driver_flush;
    disp_drv.buffer = &disp_buf;
    lv_disp_drv_register(&disp_drv);

    const esp_timer_create_args_t periodic_timer_args = {
            .callback = &lv_tick_task,
            /* name is optional, but may help identify the timer when debugging */
            .name = "periodic_gui"
    };
    esp_timer_handle_t periodic_timer;
    ESP_ERROR_CHECK(esp_timer_create(&periodic_timer_args, &periodic_timer));
    //On ESP32 it's better to create a periodic task instead of esp_register_freertos_tick_hook
    ESP_ERROR_CHECK(esp_timer_start_periodic(periodic_timer, 10 * 1000)); //10ms (expressed as microseconds)

    //
    // Create the task
    lv_task_create(lvgl_task, 100, LV_TASK_PRIO_MID, NULL);

    while (1) {
        vTaskDelay(1);
        //Try to lock the semaphore, if success, call lvgl stuff
        if (xSemaphoreTake(xGuiSemaphore, (TickType_t) 10) == pdTRUE) {
            lv_task_handler();
            xSemaphoreGive(xGuiSemaphore);
        }
    }

    //A task should NEVER return
    vTaskDelete(NULL);
}


static void lv_tick_task(void *arg) {
    (void) arg;
    lv_tick_inc(portTICK_RATE_MS);
}


static void lvgl_task(lv_task_t
                      *t) {
    State_app *state = state_app_get();

//  Create title label
    if (lvgl_objects[0] == NULL) {
        lvgl_objects[0] = lv_label_create(lv_scr_act(), NULL);
        lv_label_set_text(lvgl_objects[0], " ");

        static lv_style_t label_style;
        lv_style_copy(&label_style, &lv_style_plain);
        label_style.text.font = &lv_font_roboto_28;
        lv_label_set_style(lvgl_objects[0], LV_LABEL_STYLE_MAIN, &label_style);
    }
    //  Create subtitle label
    if (lvgl_objects[1] == NULL) {
        lvgl_objects[1] = lv_label_create(lv_scr_act(), NULL);
        lv_label_set_text(lvgl_objects[1], " ");
    }
    //  Create heading label
    if (lvgl_objects[2] == NULL) {
        lvgl_objects[2] = lv_label_create(lv_scr_act(), NULL);
        lv_label_set_text(lvgl_objects[2], " ");
    }
    lv_obj_t *title_label = lvgl_objects[0];
    lv_obj_t *sub_title_label = lvgl_objects[1];
    lv_obj_t *heading_label = lvgl_objects[2];


//    Show initial message, wifi not connected
    if (state->wifi_credentials_ready == 0) {
        lv_label_set_text(title_label, "Open the app to\nconnect to wifi");
        lv_obj_align(title_label, NULL, LV_ALIGN_CENTER, 0, 0);
    }
//  Show connecting
    else if (state->wifi_credentials_ready == 1 && state->wifi_ready == 0) {
        lv_label_set_text(title_label, "Connecting...");
        lv_obj_align(title_label, NULL, LV_ALIGN_CENTER, 0, 0);
    }
//  SHow getting time
    else if (state->wifi_ready == 1 && state->time_ready == 0) {
        lv_label_set_text(title_label, "Getting time...");
        lv_obj_align(title_label, NULL, LV_ALIGN_CENTER, 0, 0);
    }
//  Show the time & alarm time
    else if (state->time_ready == 1) {
        time_t now;
        struct tm timeinfo;
        time(&now); // get time from system
        localtime_r(&now, &timeinfo); // copy time into timeinfo struct

        char buf[100];
        snprintf(buf, 100, "%02i:%02i", timeinfo.tm_hour, timeinfo.tm_min);
        lv_label_set_text(title_label, buf);
        lv_obj_align(title_label, NULL, LV_ALIGN_CENTER, 0, 0);

        char buf2[100];
        snprintf(buf2, 100, "next alarm %02i:%02i", state->alarm_minute / 60,
                 state->alarm_minute % 60);
        lv_label_set_text(sub_title_label, buf2);
        lv_obj_align(sub_title_label, title_label, LV_ALIGN_OUT_BOTTOM_MID, 0, 0);

        char buf3[100];
        snprintf(buf3, 100, "%02i/%02i/%04i", timeinfo.tm_mday, timeinfo.tm_mon + 1, timeinfo.tm_year + 1900);
        lv_label_set_text(heading_label, buf3);
        lv_obj_align(heading_label, title_label, LV_ALIGN_OUT_TOP_MID, 0, 0);
    }
}