#include <time.h>

#ifndef STATE_APP_H
#define STATE_APP_H

typedef struct State_app {
    int wifi_credentials_ready;
    int wifi_ready;
    int time_ready;
    char tz[40];
    int alarm_minute; // minutes since 00:00

} State_app;

void state_app_init();

State_app *state_app_get();

void state_app_log();

// wifi app
void state_app_set_wifi_credentials_ready(int wifi_credentials_ready);

void state_app_set_wifi_ready(int wifi_ready);

// sntp app
void state_app_set_time_ready(int time_ready);

void state_app_set_tz(char tz[40]);

// alarm
void state_app_set_alarm_minute(int);

#endif //STATE_APP_H
