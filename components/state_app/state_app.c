#include "string.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "state_app.h"

struct State_app state;
static const char *TAG = "STATE_APP";

const char *NVS_NAME = "state_app";

void state_app_init() {
    state.wifi_credentials_ready = 0;
    state.wifi_ready = 0;
    state.time_ready = 0;
    // open nvs
    nvs_handle_t my_handle;
    nvs_open(NVS_NAME, NVS_READONLY, &my_handle);
    // try get alarm_minute
    nvs_get_i32(my_handle, "alarm_minute", &state.alarm_minute);
    // try get tz
    char tz[40];
    size_t tz_size = sizeof(tz);
    esp_err_t err;
    err = nvs_get_str(my_handle, "tz", tz, &tz_size);
    if (err == ESP_OK) {
        state_app_set_tz(tz); // update system tz
    } else {
        strcpy(tz, "<+03>-3");
        state_app_set_tz(tz);
    }
    // close nvs
    nvs_close(my_handle);
}

State_app *state_app_get() {
    return &state;
}

// wifi app
void state_app_set_wifi_credentials_ready(int wifi_credentials_ready) {
    state.wifi_credentials_ready = wifi_credentials_ready;
}

void state_app_set_wifi_ready(int wifi_ready) {
    state.wifi_ready = wifi_ready;
}

// sntp app
void state_app_set_time_ready(int time_ready) {
    state.time_ready = time_ready;
}

void state_app_set_tz(char tz[40]) {
    strcpy(state.tz, tz);
    setenv("TZ", tz, 1); // update system tz from state
    tzset(); // update C library runtime data for the new tz
    // open nvs
    nvs_handle_t my_handle;
    nvs_open(NVS_NAME, NVS_READWRITE, &my_handle);
    nvs_set_str(my_handle, "tz", tz);
    // close nvs
    nvs_close(my_handle);
}

// alarm
void state_app_set_alarm_minute(int minute) {
    state.alarm_minute = minute;
    // open nvs
    nvs_handle_t my_handle;
    nvs_open(NVS_NAME, NVS_READWRITE, &my_handle);
    nvs_set_i32(my_handle, "alarm_minute", minute);
    // close nvs
    nvs_close(my_handle);
}

//
void state_app_log() {
    ESP_LOGI(TAG, "*********************************************************************************************");
    ESP_LOGI(TAG, "wifi_credentials_ready:%i", state.wifi_credentials_ready);
    ESP_LOGI(TAG, "wifi_ready:%i", state.wifi_ready);
    ESP_LOGI(TAG, "time_ready:%i", state.time_ready);
    ESP_LOGI(TAG, "tz:%s", state.tz);
    time_t now;
    struct tm timeinfo;
    time(&now); // get time from system
    localtime_r(&now, &timeinfo); // copy time into timeinfo struct
    ESP_LOGI(TAG, "date %i-%i-%i", timeinfo.tm_mday, timeinfo.tm_mon + 1, timeinfo.tm_year + 1900);
    ESP_LOGI(TAG, "time %02i:%02i", timeinfo.tm_hour, timeinfo.tm_min);
    ESP_LOGI(TAG, "alarm %02i:%02i", state.alarm_minute / 60, state.alarm_minute % 60);
    ESP_LOGI(TAG, "*********************************************************************************************");
}
