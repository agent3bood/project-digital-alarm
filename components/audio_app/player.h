#include <stdint.h>

#ifndef DIGITAL_ALARM_PLAYER_H
#define DIGITAL_ALARM_PLAYER_H

#define BUF_SIZE (10)

void fill_uint16_bigend(uint8_t *thebuf, uint16_t data);

uint16_t player_get_checksum(uint8_t *thebuf);

void player_fill_checksum();

void recv_func();

void send_func();

void player_send_cmd(uint8_t cmd, uint8_t high_arg, uint8_t low_arg);

void player_set_volume(uint16_t volume);

void player_play();

void player_pause();

void player_reset();

void player_repeat_on();


#endif //DIGITAL_ALARM_PLAYER_H
