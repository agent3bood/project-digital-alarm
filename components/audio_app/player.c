#include "player.h"
#include <stdint.h>
#include "driver/uart.h"
#include "audio_app.h"
#include "esp_log.h"


uint8_t send_buf[10] = {0x7E, 0xFF, 06, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0xEF};
// 7E FF 06 0F 00 01 01 xx xx EF
// 0	->	7E start code
// 1	->	FF version
// 2	->	06 data length (not counting start, end, verification)
// 3	->	0F is command
// 4	->	00 is no receive feedback, 01 is receive feedback
// 5~6	->	01 01 is argument
// 7~8	->	checksum = 0 - ( FF+06+0F+00+01+01 )
// 9	->	EF is end code

uint8_t recv_buf[10];

//
void fill_uint16_bigend(uint8_t *thebuf, uint16_t data) {
    *thebuf = (uint8_t) (data >> 8);
    *(thebuf + 1) = (uint8_t) data;
}

//calc checksum (1~6 byte)
uint16_t player_get_checksum(uint8_t *thebuf) {
    uint16_t sum = 0;
    for (int i = 1; i < 7; i++) {
        sum += thebuf[i];
    }
    return -sum;
}


//fill checksum to send_buf (7~8 byte)
void player_fill_checksum() {
    uint16_t checksum = player_get_checksum(send_buf);
    fill_uint16_bigend(send_buf + 7, checksum);
}

// Receive data
void recv_func() {
    uart_read_bytes(UART_NUM, recv_buf, BUF_SIZE, 20 / portTICK_RATE_MS);
    ESP_LOGI(TAG, "received");
    ESP_LOG_BUFFER_HEX(TAG, recv_buf, BUF_SIZE);
    uart_flush(UART_NUM);
}


// Send the send_buf
void send_func() {
    ESP_LOGI(TAG, "sending");
    ESP_LOG_BUFFER_HEX(TAG, send_buf, BUF_SIZE);
    uart_write_bytes(UART_NUM, (const char *) send_buf, BUF_SIZE);
}

// Send the command to DFPlayer
void player_send_cmd(uint8_t cmd, uint8_t high_arg, uint8_t low_arg) {
    // delay
    vTaskDelay(1000 / portTICK_PERIOD_MS);

    // fill send_buf
    send_buf[3] = cmd;
    send_buf[5] = high_arg;
    send_buf[6] = low_arg;

    player_fill_checksum();
    send_func();
    // delay
    vTaskDelay(100 / portTICK_PERIOD_MS);
    recv_func();
}

//0x06 set volume 0-30
void player_set_volume(uint16_t volume) {
    player_send_cmd(0x06, 0x00, volume);
}

// Play audio
void player_play() {
    player_send_cmd(0x0d, 0x00, 0x00);
}

// Pause audio
void player_pause() {
    player_send_cmd(0x0e, 0x00, 0x00);
}

// Reset the DFPlayer
void player_reset() {
    player_send_cmd(0x0c, 0x00, 0x00);
}

// Play audio and loop
void player_repeat_on() {
    player_send_cmd(0x11, 0x00, 0x01);
}
