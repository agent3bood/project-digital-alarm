#ifndef DIGITAL_ALARM_AUDIO_APP_H
#define DIGITAL_ALARM_AUDIO_APP_H

static const char *TAG = "AUDIO_APP";
#define UART_NUM (UART_NUM_2)
#define TX  (GPIO_NUM_17)
#define RX  (GPIO_NUM_16)
#define RTS  (UART_PIN_NO_CHANGE)
#define CTS  (UART_PIN_NO_CHANGE)

void audio_app(void *pvParameters);

#endif //DIGITAL_ALARM_AUDIO_APP_H
