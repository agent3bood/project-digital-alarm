#include "audio_app.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/uart.h"
#include "driver/gpio.h"
#include "esp_log.h"
#include "state_app.h"
#include "player.h"
#include "driver/touch_pad.h"

void audio_app(void *pvParameters) {
    State_app *state = state_app_get();
    int audio_playing = 0;
    time_t now;
    struct tm timeinfo;

    uint16_t touch_value;



//  Configure UART
    uart_config_t uart_config = {
            .baud_rate = 9600,
            .data_bits = UART_DATA_8_BITS,
            .parity    = UART_PARITY_DISABLE,
            .stop_bits = UART_STOP_BITS_1,
            .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
            .source_clk = UART_SCLK_APB,
    };
    uart_param_config(UART_NUM, &uart_config);
    uart_set_pin(UART_NUM, TX, RX, RTS, CTS);
    // initialize driver
    uart_driver_install(UART_NUM, 2048
                                  * 2, 0, 0, NULL, 0);

//   Reset DFPlayer
    player_reset();

//   Main loop, check alarm time
    while (1) {
        time(&now); // get time from system
        localtime_r(&now, &timeinfo); // copy time into timeinfo struct
        if (!audio_playing && state->time_ready && state->alarm_minute == (timeinfo.tm_hour * 60 + timeinfo.tm_min)) {
            audio_playing = 1;
//          It is time play the audio
            player_reset();
            player_set_volume(15);
            player_repeat_on(); // this will start on repeat mode
        }
//      Reset audio_playing after alarm time has passed
        if (audio_playing && state->time_ready && state->alarm_minute < (timeinfo.tm_hour * 60 + timeinfo.tm_min)) {
            audio_playing = 0;
        }
//      Listen for captive touch to stop the audio
        if (audio_playing) {
            touch_pad_read(TOUCH_PAD_NUM8, &touch_value);
            ESP_LOGI(TAG, "T%d:[%4d] ", TOUCH_PAD_NUM8, touch_value);
            // touched sensor will be bellow 500
            // if sensor is touched stop the player
            if (touch_value < 500) {
                player_pause();
                player_reset();
            }
        }
        vTaskDelay(10);
    }

//  Task should never exit
    vTaskDelete(NULL);
}