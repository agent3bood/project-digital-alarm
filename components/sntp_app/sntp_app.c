#include "sntp_app.h"
#include "../state_app/state_app.h"
#include <time.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "esp_attr.h"
#include "esp_sntp.h"

static const char *TAG = "SNTP_APP";

void sntp_app(void *pvParameters) {
    State_app *state = state_app_get();
    time_t now;
    struct tm timeinfo;

    while (!state->wifi_ready) {
        ESP_LOGI(TAG, "Waiting for WIFI");
        vTaskDelay(1000);
    }

    while (1) {
        time(&now); // get time from system
        localtime_r(&now, &timeinfo); // copy time into timeinfo struct
        // Is time set? If not, tm_year will be (1970 - 1900).
        if (timeinfo.tm_year < (2019 - 1900)) {
            ESP_LOGI(TAG, "Time is not set yet.");
            ESP_LOGI(TAG, "Initializing SNTP");
            sntp_setoperatingmode(SNTP_OPMODE_POLL);
            sntp_setservername(0, "pool.ntp.org");
            sntp_set_sync_mode(SNTP_SYNC_MODE_SMOOTH);
            sntp_init();
            // wait for SNTP to finish
            while (sntp_get_sync_status() == SNTP_SYNC_STATUS_RESET) {
                ESP_LOGI(TAG, "Waiting for system time to be set...");
                vTaskDelay(1000);
            }
            //
            state_app_set_tz(state->tz); // update system tz
            state_app_set_time_ready(1); // update state
        } else {
            // time is ok, delay and check again
            vTaskDelay(1000);
        }
    }
}