#include "http_server_app.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "state_app.h"
#include "esp_http_server.h"
#include "cJSON.h"
#include "esp_log.h"

static const char *TAG = "HTTP_SERVER_APP";

// HTTP GET handler for /state
static esp_err_t get_state_handler(httpd_req_t *req) {
    State_app *state = state_app_get();

    cJSON *root = NULL;
    char *resp = NULL;
//  Create root json object
    root = cJSON_CreateObject();
//  Add fields to root object
    cJSON_AddItemToObject(root, "wifi_credentials_ready", cJSON_CreateBool(state->wifi_credentials_ready));
    cJSON_AddItemToObject(root, "wifi_ready", cJSON_CreateBool(state->wifi_ready));
    cJSON_AddItemToObject(root, "time_ready", cJSON_CreateBool(state->time_ready));
    //
    time_t now;
    time(&now); // get time from system
    cJSON_AddItemToObject(root, "time", cJSON_CreateNumber(now));
    //
    cJSON_AddItemToObject(root, "tz", cJSON_CreateString(state->tz));
    //
    cJSON_AddItemToObject(root, "alarm_minute", cJSON_CreateNumber(state->alarm_minute));
//  Convert root json object to string (encoding)
    resp = cJSON_Print(root);
//  Send the encoded json string
    httpd_resp_send(req, resp, strlen(resp));
//  Delete the root json object to free its memory
    cJSON_Delete(root);
//  Return
    return ESP_OK;
}

// HTTP POST handler for /state
static esp_err_t post_state_handler(httpd_req_t *req) {
//   initialize json fields
    cJSON *root = NULL;
    cJSON *alarm_minute = NULL;
    cJSON *tz = NULL;
//  Buffer to hold incoming request body
    char content[1024];
//  Truncate if content length larger than the buffer
    size_t recv_size = MIN(req->content_len, sizeof(content));
    //
    int ret = httpd_req_recv(req, content, recv_size);
    if (ret <= 0) { /* 0 return value indicates connection closed */
        /* Check if timeout occurred */
        if (ret == HTTPD_SOCK_ERR_TIMEOUT) {
            /* In case of timeout one can choose to retry calling
             * httpd_req_recv(), but to keep it simple, here we
             * respond with an HTTP 408 (Request Timeout) error */
            httpd_resp_send_408(req);
        }
        /* In case of error, returning ESP_FAIL will
         * ensure that the underlying socket is closed */
        goto error;
    }
    //
    ESP_LOGI(TAG, "=========== RECEIVED DATA ==========");
    ESP_LOGI(TAG, "%.*s", ret, content);
    ESP_LOGI(TAG, "====================================");
//  Parse content as json (decode)
    root = cJSON_Parse(content);
    if (root == NULL) {
        printf("Error before: [%s]\n", cJSON_GetErrorPtr());
        goto error;
    }
//  Update app state
    else {
        alarm_minute = cJSON_GetObjectItemCaseSensitive(root, "alarm_minute");
        if (cJSON_IsNumber(alarm_minute)) {
            state_app_set_alarm_minute((int) alarm_minute->valuedouble);
        }
        //
        tz = cJSON_GetObjectItemCaseSensitive(root, "tz");
        if (cJSON_IsString(tz) && (tz->valuestring != NULL)) {
            state_app_set_tz(tz->valuestring);
        }
        //
        goto ok;
    }
//
    ok:
//  Respond with an empty chunk to signal HTTP response completion
    httpd_resp_send(req, NULL, 0);
    cJSON_Delete(root);
    return ESP_OK;
    error:
//  Respond with an empty chunk to signal HTTP response completion
    httpd_resp_set_status(req, "500");
    httpd_resp_send(req, NULL, 0);
    cJSON_Delete(root);
    return ESP_FAIL;
}

static const httpd_uri_t get_state = {
        .uri = "/state",
        .method = HTTP_GET,
        .handler = get_state_handler
};

static const httpd_uri_t post_state = {
        .uri = "/state",
        .method = HTTP_POST,
        .handler = post_state_handler
};

// Start the web server
static httpd_handle_t start_webserver(void) {
    httpd_handle_t server = NULL;
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();

    // Start the httpd server
    ESP_LOGI(TAG, "Starting server on port: '%d'", config.server_port);
    if (httpd_start(&server, &config) == ESP_OK) {
        // Set URI handlers
        ESP_LOGI(TAG, "Registering URI handlers");
        httpd_register_uri_handler(server, &get_state);
        httpd_register_uri_handler(server, &post_state);
        return server;
    }

    ESP_LOGI(TAG, "Error starting server!");
    return NULL;
}

void http_server_app(void *pvParameters) {
    start_webserver();
    while (1) {}
//  Tasks should never exit
    vTaskDelete(NULL);
}


