#ifndef DIGITAL_ALARM_HTTP_SERVER_APP_H
#define DIGITAL_ALARM_HTTP_SERVER_APP_H

void http_server_app(void *pvParameters);

#endif //DIGITAL_ALARM_HTTP_SERVER_APP_H

#ifndef MIN
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#endif
#ifndef MAX
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#endif